//
//  ProfileViewController.swift
//  FitnessApp
//
//  Created by Ron Ramirez on 5/26/17.
//  Copyright © 2017 Ron Ramirez. All rights reserved.
//

import UIKit
import SDWebImage
import Pastel
import KDCircularProgress
import Firebase

class ProfileViewController: UIViewController {
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var profileImageView: CircleImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var logOutButton: UIButton!
    
    @IBOutlet weak var currentNavigationItem: UINavigationItem!
    
    @IBOutlet weak var addCommentTextField: UITextField!
    @IBOutlet weak var stepsCounterProgressView: KDCircularProgress!
    @IBOutlet weak var totalStepsLabel: UILabel!
    @IBOutlet weak var totalCaloriesLabel: UILabel!
    @IBOutlet weak var averageHeartRate: UILabel!
    @IBOutlet weak var averageBodyTemperature: UILabel!
    @IBOutlet weak var likesCountLabel: UILabel!
    @IBOutlet weak var dislikesCountLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    //Represents the state of the view controller
    //nil means current user / not nil means other users
    var currentUserKey : String?
    var currentDate = Date()
    
    //Firebase
    var messages = [Message]()
    
    
    //MARK: View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPastel()
        
        if let otherUserKey = currentUserKey {
            print("Other user keys -" , otherUserKey)
            loadCurrentUsersProfileBasedOn(userKey: otherUserKey)
            self.backButton.isHidden = false
            self.logOutButton.isHidden = true
            self.title = "Friend User Profile"
        }else {
            self.backButton.isHidden = true
            loadCurrentUsersProfileBasedOn(userKey: DataService.ds.REF_USER_CURRENT.key)
            //Assign user key for messages
            currentUserKey = DataService.ds.REF_USER_CURRENT.key
        }
        
        //Add observer for messages
        observeMessageForProfile()
        observeLikeLabels()
    }
    
    //MARK: IBActions
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func postButtonPressed(_ sender: UIButton) {
        // Post Button Pressed
        guard let commentText = self.addCommentTextField.text, commentText != "" else {
            print("Insert comment text before posting!")
            return
        }
        
        //No current user key
        guard let currentKey = currentUserKey else {
            print("No current user key exists")
            return
        }
        
        let currentDate = Date()
        
        //Create message dictionary for each user
        let messageDict = ["message" : commentText,
                           "senderKey" : DataService.ds.REF_USER_CURRENT.key,
                           "timeStamp" : currentDate.CommentDateFormatter()]
        
        print("Message Dict", messageDict)
        
        //Update Firebase
        DataService.ds.REF_PUBLIC_USERS.child(currentKey).child("fitnessPosts").child(currentDate.FirebaseDateFormatter()).child("messages").childByAutoId().updateChildValues(messageDict)
        
        //Clear text
        self.addCommentTextField.text = ""
    }
    
    @IBAction func likeButtonPressed(_ sender: UIButton) {
        
        //No current user key
        guard let currentKey = currentUserKey else {
            print("No current user key exists")
            return
        }
        
        //Set likes
        DataService.ds.REF_PUBLIC_USERS.child(currentKey).child("fitnessPosts").child(currentDate.FirebaseDateFormatter()).child("likes").child(DataService.ds.REF_USER_CURRENT.key).setValue(true)
        
        //Set dislikes
        DataService.ds.REF_PUBLIC_USERS.child(currentKey).child("fitnessPosts").child(currentDate.FirebaseDateFormatter()).child("dislikes").child(DataService.ds.REF_USER_CURRENT.key).removeValue()
        
    }
    
    @IBAction func dislikeButtonPressed(_ sender: UIButton) {
        
        //No current user key
        guard let currentKey = currentUserKey else {
            print("No current user key exists")
            return
        }
        
        //Set dislikes
        DataService.ds.REF_PUBLIC_USERS.child(currentKey).child("fitnessPosts").child(currentDate.FirebaseDateFormatter()).child("dislikes").child(DataService.ds.REF_USER_CURRENT.key).setValue(true)
        
        //Set likes
        DataService.ds.REF_PUBLIC_USERS.child(currentKey).child("fitnessPosts").child(currentDate.FirebaseDateFormatter()).child("likes").child(DataService.ds.REF_USER_CURRENT.key).removeValue()
    }
    
    
    
    
    //MARK: UIView Functions
    
    func setupPastel() {
        let pastelView = PastelView(frame: view.bounds)
        
        // Custom Direction
        pastelView.startPastelPoint = .bottomLeft
        pastelView.endPastelPoint = .topRight
        
        // Custom Duration
        pastelView.animationDuration = 3.0
        
        // Custom Color
        pastelView.setColors([
            UIColor(red: 90/255, green: 120/255, blue: 127/255, alpha: 1.0),
            UIColor(red: 58/255, green: 255/255, blue: 217/255, alpha: 1.0)])
        
        pastelView.startAnimation()
        view.insertSubview(pastelView, at: 0)
    }
    
    //MARK: Firebase Functions
    
    func observeLikeLabels() {
        //No current user key
        guard let currentKey = currentUserKey else {
            print("No current user key exists")
            return
        }
        
        DataService.ds.REF_PUBLIC_USERS.child(currentKey).child("fitnessPosts").child(currentDate.FirebaseDateFormatter()).child("likes").observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                let likesCount = snapshot.childrenCount
                self.likesCountLabel.text = "\(likesCount)"
            }else {
                self.likesCountLabel.text = "0"
            }
        })
        
        DataService.ds.REF_PUBLIC_USERS.child(currentKey).child("fitnessPosts").child(currentDate.FirebaseDateFormatter()).child("dislikes").observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                let dislikesCount = snapshot.childrenCount
                self.dislikesCountLabel.text = "\(dislikesCount)"
            } else{
                self.dislikesCountLabel.text = "0"
            }
        })
    }
    func loadCurrentUsersProfileBasedOn(userKey: String) {
        
        //Load user info
        DataService.ds.REF_PUBLIC_USERS.child(userKey).observeSingleEvent(of: .value, with: { (snapshot) in
            if let currentUserDict = snapshot.value as? Dictionary<String, AnyObject> {
                
                //Access name
                if let userName = currentUserDict["name"] as? String {
                    self.profileNameLabel.text = userName
                }
                
                //Retrieve user Image
                if let imageURL = currentUserDict["imageURL"] as? String {
                    print("User  Image URL \(imageURL)")
                    
                    self.profileImageView.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "placeholder.png"))
                }
            }
        })
        
        //Load fitness profile based on date
        //Load user info
        DataService.ds.REF_PUBLIC_USERS.child(userKey).child("fitnessPosts").child(currentDate.FirebaseDateFormatter()).observe(.value, with: { (snapshot) in
            print("FITNESS", snapshot)
            if let fitnessDataDict = snapshot.value as? Dictionary<String, AnyObject> {
                
                //Access Total Steps
                if let totalSteps = fitnessDataDict["totalSteps"] as? Int {
                    self.totalStepsLabel.text = "\(totalSteps)"
                    self.totalCaloriesLabel.text = "\(Int(Double(totalSteps) * 0.045))"
                    self.stepsCounterProgressView.angle = Double((totalSteps * 360) / 10000)
                }
                
                //Average Heart Rate
                if let averageHeartRate = fitnessDataDict["averageHeartRate"] as? Double {
                    self.averageHeartRate.text = "\(Int(averageHeartRate))"
                }
                
                //Body Temperature
                if let averageBodyTemp = fitnessDataDict["averageBodyTemp"] as? Double {
                    self.averageBodyTemperature.text = "\(Int(averageBodyTemp))"
                }
            }
        })
    }
    
    func observeMessageForProfile() {
        
        //No current user key
        guard let currentKey = currentUserKey else {
            print("No current user key exists")
            return
        }
        
        DataService.ds.REF_PUBLIC_USERS.child(currentKey).child("fitnessPosts").child(currentDate.FirebaseDateFormatter()).child("messages").observe(.value, with: { (snapshot) in
            
            if snapshot.exists() {
                if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                    //Clear all user keys to prevent duplicates
                    self.messages = []
                    for snap in snapshots {
                        if let messageDict = snap.value as? Dictionary<String, AnyObject> {
                            let message = Message(messageData: messageDict)
                            self.messages.append(message)
                        }
                    }
                    self.tableView.reloadData()
                }
            }
        })
    }
}


extension ProfileViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = self.messages[indexPath.row]
        
        if let messageCell = tableView.dequeueReusableCell(withIdentifier: "messageCell") as? MessageTableViewCell {
            messageCell.configureCellWith(message: message)
            return messageCell
        }
        return UITableViewCell()
    }
    
}
