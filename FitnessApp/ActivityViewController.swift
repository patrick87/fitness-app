//
//  ActivityViewController.swift
//  FitnessApp
//
//  Created by Ron Ramirez on 5/21/17.
//  Copyright © 2017 Ron Ramirez. All rights reserved.
//

import UIKit
import SwiftCharts
import CoreData


class ActivityViewController: UIViewController {
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var weekLabel: UILabel!
    @IBOutlet weak var chartsView: UIView!
    fileprivate var monthlyChart: Chart? // arc
    fileprivate var weeklyChart: Chart? // arc
    
    // Fetch Results
    var weeklyResultsArray = [FitnessData]()
    var monthlyResultsArray = [FitnessData]()
    var currentDate = Date()
    var isCheckingMonths = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Set up label
        self.monthLabel.text = currentDate.MonthDateFormatter()
        
        //Load up Core Data Values
        loadAllDataWithinMonth()
        loadAllDataWithinWeek()
        
        for fitness in self.weeklyResultsArray {
            let date : Date = fitness.date! as Date
            print("Fitness Data - ", date.MonthDayDateFormatter(), fitness.totalSteps, fitness.totalCalories)
        }
        
        //        for fitness in self.monthlyResultsArray {
        //            let date : Date = fitness.date! as Date
        //            print("Fitness Data - ", date.MonthDayDateFormatter(), fitness.totalSteps, fitness.totalCalories)
        //        }
        
        //Initialize with weeklyChart
        self.loadWeeklyChart()
    }
    
    //MARK: IBActions
    @IBAction func nextDateButtonPressed(_ sender: UIButton) {
        if isCheckingMonths {
            self.currentDate = (Calendar.current as NSCalendar).date(byAdding: .month, value: 1, to: currentDate, options: [])!
            loadAllDataWithinMonth()
            loadMonthChart()
        }else  {
            self.currentDate = (Calendar.current as NSCalendar).date(byAdding: .day, value: 7, to: currentDate, options: [])!
            loadAllDataWithinWeek()
            loadWeeklyChart()
        }
    }

    
    @IBAction func lastDateButtonPressed(_ sender: UIButton) {
        if isCheckingMonths {
            self.currentDate = (Calendar.current as NSCalendar).date(byAdding: .month, value: -1, to: currentDate, options: [])!
            loadAllDataWithinMonth()
            loadMonthChart()
        }else  {
            self.currentDate = (Calendar.current as NSCalendar).date(byAdding: .day, value: -7, to: currentDate, options: [])!
            loadAllDataWithinWeek()
            loadWeeklyChart()
        }
    }
    @IBAction func segmentControlValueChanged(_ sender: UISegmentedControl) {
        //Reset date
        self.currentDate = Date()
        
        if (sender.selectedSegmentIndex == 0) {
            loadWeeklyChart()
            self.monthLabel.isHidden = true
            self.weekLabel.isHidden = false
            isCheckingMonths = false
            
        }else {
            loadMonthChart()
            self.monthLabel.isHidden = false
            self.weekLabel.isHidden = true
            isCheckingMonths = true
        }
    }
    
    //MARK: SwiftCharts
    
    func loadMonthChart() {
        let labelSettings = ChartLabelSettings(font: ExamplesDefaults.labelFont)
        
        guard self.monthlyResultsArray.count > 5 else {
            print("No Month data to load")
            return
        }
        
        let monthDate = (self.monthlyResultsArray.first!.date!) as Date
        //Load month charts
        self.monthLabel.text = "\(monthDate.MonthDateFormatter())"
        
        var monthlyChartPoints : [(Int, Int)] = self.monthlyResultsArray.enumerated().map { (index, fitnessData) in
            return (index, Int(fitnessData.totalSteps))
        }
        
        monthlyChartPoints.insert((0,0), at: 0)
        
        //Monthly Chart Points
        let chartPoints = monthlyChartPoints.map{ChartPoint(x: ChartAxisValueInt($0.0, labelSettings: labelSettings), y: ChartAxisValueInt($0.1))}
        
        
        let xValues = chartPoints.map{$0.x}
        
        let yValues = ChartAxisValuesStaticGenerator.generateYAxisValuesWithChartPoints(chartPoints, minSegmentCount: 10, maxSegmentCount: 1000, multiple: 2000, axisValueGenerator: {ChartAxisValueDouble($0, labelSettings: labelSettings)}, addPaddingSegmentIfEdge: false)
        
        let xModel = ChartAxisModel(axisValues: xValues, axisTitleLabel: ChartAxisLabel(text: "Daily Steps Count", settings: labelSettings))
        let yModel = ChartAxisModel(axisValues: yValues, axisTitleLabel: ChartAxisLabel(text: "Steps Count", settings: labelSettings.defaultVertical()))
        
        let chartFrame = self.chartsView.frame
        let chartSettings = ExamplesDefaults.chartSettingsWithPanZoom
        
        let coordsSpace = ChartCoordsSpaceLeftBottomSingleAxis(chartSettings: chartSettings, chartFrame: chartFrame, xModel: xModel, yModel: yModel)
        
        let (xAxisLayer, yAxisLayer, innerFrame) = (coordsSpace.xAxisLayer, coordsSpace.yAxisLayer, coordsSpace.chartInnerFrame)
        
        let lineModel = ChartLineModel(chartPoints: chartPoints, lineColor: themeColor, lineWidth: 5, animDuration: 1, animDelay: 0)
        
        let chartPointsLineLayer = ChartPointsLineLayer(xAxis: xAxisLayer.axis, yAxis: yAxisLayer.axis, lineModels: [lineModel], pathGenerator: CatmullPathGenerator())
        
        let settings = ChartGuideLinesDottedLayerSettings(linesColor: UIColor.black, linesWidth: ExamplesDefaults.guidelinesWidth)
        let guidelinesLayer = ChartGuideLinesDottedLayer(xAxisLayer: xAxisLayer, yAxisLayer: yAxisLayer, settings: settings)
        
        let chart = Chart(
            frame: chartFrame,
            innerFrame: innerFrame,
            settings: chartSettings,
            layers: [
                xAxisLayer,
                yAxisLayer,
                guidelinesLayer,
                chartPointsLineLayer
            ]
        )
        
        
        //Clear all charts
        self.weeklyChart?.view.removeFromSuperview()
        self.monthlyChart?.view.removeFromSuperview()
        
        self.monthlyChart = chart
        
        //Month Chart was just initialize so it's safe to force unwrapped
        self.view.addSubview((self.monthlyChart?.view)!)
    }
    
    func loadWeeklyChart() {
        
        guard self.weeklyResultsArray.count > 5 else {
            print("No data to load")
            return
        }
        
        //Set up label
        let firstDayOfTheWeekdate  = (self.weeklyResultsArray.first!.date!) as Date
        let lastDayOfTheWeekdate = (self.weeklyResultsArray.last!.date!) as Date
        
        
        self.weekLabel.text = "\(firstDayOfTheWeekdate.MonthDayDateFormatter()) - \(lastDayOfTheWeekdate.MonthDayDateFormatter())"
        
        
        let labelSettings = ChartLabelSettings(font: ExamplesDefaults.labelFont)
        
        //Map the last 7 days into Core Data
        let daysStrings : [String] = self.weeklyResultsArray.map { (fitnessData) in
            let date = fitnessData.date! as Date
            return date.DayDateFormatter()
        }
        
        var dayArray = [Day]()
        for (index,day) in daysStrings.enumerated() {
            dayArray.append(Day(name: day, quantity: StepsQuantity(number: Int(self.weeklyResultsArray[index].totalSteps))))
        }
        
        //Weekly Chart Points
        let weeklyChartPoints: [ChartPoint] = dayArray.enumerated().map {index, item in
            let xLabelSettings = ChartLabelSettings(font: ExamplesDefaults.labelFont, rotation: 45, rotationKeep: .top)
            let x = ChartAxisValueString(item.name, order: index, labelSettings: xLabelSettings)
            let y = ChartAxisValueString(order: item.quantity.number, labelSettings: labelSettings)
            return ChartPoint(x: x, y: y)
        }
        
        let xValues = [ChartAxisValueString("", order: -1)] + weeklyChartPoints.map{$0.x} + [ChartAxisValueString("", order: 7)]
        
        //Monthly
        //let xValues = chartPoints.map{$0.x}
        
        let yValues = ChartAxisValuesStaticGenerator.generateYAxisValuesWithChartPoints(weeklyChartPoints, minSegmentCount: 10, maxSegmentCount: 10000, multiple: 1000, axisValueGenerator: {ChartAxisValueDouble($0, labelSettings: labelSettings)}, addPaddingSegmentIfEdge: false)
        
        let xModel = ChartAxisModel(axisValues: xValues, axisTitleLabel: ChartAxisLabel(text: "Weekly Steps Count", settings: labelSettings))
        let yModel = ChartAxisModel(axisValues: yValues, axisTitleLabel: ChartAxisLabel(text: "Steps Count", settings: labelSettings.defaultVertical()))
        
        //let chartFrame = ExamplesDefaults.chartFrame(view.bounds)
        let chartFrame = self.chartsView.frame
        let chartSettings = ExamplesDefaults.chartSettingsWithPanZoom
        
        let coordsSpace = ChartCoordsSpaceLeftBottomSingleAxis(chartSettings: chartSettings, chartFrame: chartFrame, xModel: xModel, yModel: yModel)
        
        let (xAxisLayer, yAxisLayer, innerFrame) = (coordsSpace.xAxisLayer, coordsSpace.yAxisLayer, coordsSpace.chartInnerFrame)
        
        let lineModel = ChartLineModel(chartPoints: weeklyChartPoints, lineColor: themeColor, lineWidth: 5, animDuration: 1, animDelay: 0)
        
        let chartPointsLineLayer = ChartPointsLineLayer(xAxis: xAxisLayer.axis, yAxis: yAxisLayer.axis, lineModels: [lineModel], pathGenerator: CatmullPathGenerator()) // || CubicLinePathGenerator
        
        let settings = ChartGuideLinesDottedLayerSettings(linesColor: UIColor.black, linesWidth: ExamplesDefaults.guidelinesWidth)
        let guidelinesLayer = ChartGuideLinesDottedLayer(xAxisLayer: xAxisLayer, yAxisLayer: yAxisLayer, settings: settings)
        
        let chart = Chart(
            frame: chartFrame,
            innerFrame: innerFrame,
            settings: chartSettings,
            layers: [
                xAxisLayer,
                yAxisLayer,
                guidelinesLayer,
                chartPointsLineLayer
            ]
        )
        
        //Clear All Charts
        self.monthlyChart?.view.removeFromSuperview()
        self.weeklyChart?.view.removeFromSuperview()
        
        self.weeklyChart = chart
        
        //Safe to unrwapped since it's weeklyChart was just initialzied
        self.view.addSubview((self.weeklyChart?.view)!)
    }
    
    //Core Data functions
    func loadAllDataWithinWeek() {
        print("Load Fitness Data Based on Week")
        let app = UIApplication.shared.delegate as! AppDelegate
        let context = app.persistentContainer.viewContext //scratch pad
        let currentFitnessDataFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "FitnessData")
        
        // Get the current calendar with local time zone
        var calendar = Calendar.current
        calendar.timeZone = NSTimeZone.local
        
        // Get today's beginning & end
        let calculatedDate = (Calendar.current as NSCalendar).date(byAdding: .day, value: 1, to: currentDate, options: [])!
        
        let dateFrom = calendar.startOfDay(for: calculatedDate) // eg. 2016-10-10 00:00:00
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute],from: dateFrom)
        components.day! += -7
        let dateTo = calendar.date(from: components)! // eg. 2016-10-11 00:00:00
        // Note: Times are printed in UTC. Depending on where you live it won't print 00:00:00 but it will work with UTC times which can be converted to local time
        
        // Set predicate as date being today's date
        let datePredicate = NSPredicate(format: "(%@ >= date) AND (date >= %@)", argumentArray: [dateFrom, dateTo])
        
        //Sort descriptors
        let sectionSortDescriptor = NSSortDescriptor(key: "date", ascending: true)
        let sortDescriptors = [sectionSortDescriptor]
        
        currentFitnessDataFetch.sortDescriptors = sortDescriptors
        currentFitnessDataFetch.predicate = datePredicate
        
        do {
            print("Do Block")
            let fetchedFitnessData = try context.fetch(currentFitnessDataFetch) as! [FitnessData]
            self.weeklyResultsArray = fetchedFitnessData
            print(fetchedFitnessData)
        } catch {
            fatalError("Failed to fetch current fitness data: \(error.localizedDescription)")
        }
    }
    
    func loadAllDataWithinMonth() {
        print("Load Fitness Data Based on Month")
        let app = UIApplication.shared.delegate as! AppDelegate
        let context = app.persistentContainer.viewContext //scratch pad
        let currentFitnessDataFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "FitnessData")
        
        // Get the current calendar with local time zone
        var calendar = Calendar.current
        calendar.timeZone = NSTimeZone.local
        
        // Get today's beginning & end
        let dateFrom = calendar.startOfDay(for: currentDate) // eg. 2016-10-10 00:00:00
        var components = calendar.dateComponents([.year, .month],from: dateFrom)
        //components.month! += 1
        let dateTo = calendar.date(from: components)! // eg. 2016-10-11 00:00:00
        // Note: Times are printed in UTC. Depending on where you live it won't print 00:00:00 but it will work with UTC times which can be converted to local time
        
        // Set predicate as date being today's date
        let datePredicate = NSPredicate(format: "(%@ >= date) AND (date > %@)", argumentArray: [dateFrom, dateTo])
        
        //Sort descriptors
        let sectionSortDescriptor = NSSortDescriptor(key: "date", ascending: true)
        let sortDescriptors = [sectionSortDescriptor]
        
        
        currentFitnessDataFetch.sortDescriptors = sortDescriptors
        currentFitnessDataFetch.predicate = datePredicate
        
        
        for fitness in self.monthlyResultsArray {
            let date : Date = fitness.date! as Date
            print("Fitness Data - ", date.MonthDayDateFormatter(), fitness.totalSteps, fitness.totalCalories)
        }
        do {
            print("Do Block")
            let fetchedFitnessData = try context.fetch(currentFitnessDataFetch) as! [FitnessData]
            self.monthlyResultsArray = fetchedFitnessData
            
            
            for fitness in fetchedFitnessData {
                let date : Date = fitness.date! as Date
                print("Fitness Data - ", date.MonthDayDateFormatter(), fitness.totalSteps, fitness.totalCalories)
            }
        } catch {
            fatalError("Failed to fetch current fitness data: \(error.localizedDescription)")
        }
    }
}

private struct StepsQuantity {
    let number: Int
    
    init(number: Int) {
        self.number = number
    }
}

private struct Day{
    let name: String
    let quantity: StepsQuantity
    
    init(name: String, quantity: StepsQuantity) {
        self.name = name
        self.quantity = quantity
    }
}
