//
//  RewardTableViewCell.swift
//  FitnessApp
//
//  Created by Ron Ramirez on 5/30/17.
//  Copyright © 2017 Ron Ramirez. All rights reserved.
//

import UIKit
import KDCircularProgress

class RewardTableViewCell: UITableViewCell {
    @IBOutlet weak var stepsProgressView: KDCircularProgress!
    @IBOutlet weak var rewardImageView: UIImageView!

    @IBOutlet weak var currentStepsLabel: UILabel!
    @IBOutlet weak var monthDateLabel: UILabel!
    @IBOutlet weak var weekdayDateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCellWith(fitnessData : FitnessData) {
        
        //Setup weekday label
        let date = fitnessData.date! as Date
        self.weekdayDateLabel.text = date.WeekdayDateFormatter()
        self.monthDateLabel.text = date.MonthDayDateFormatter()
        
        //Set up steps label
        self.currentStepsLabel.text = "\(fitnessData.totalSteps)"
        
        //Set up progress bar
        let currentSteps = Int(fitnessData.totalSteps)
        self.stepsProgressView.angle = Double((currentSteps * 360) / 10000)
        
        
        //Handle achievement image
        if (currentSteps >= 10000) {

            let darkerThemeColor = UIColor(red: 90.0 / 255.0, green: 188.0 / 255.0, blue:  209.0 / 255.0, alpha: 0.5)
            self.stepsProgressView.trackColor = darkerThemeColor
            self.rewardImageView.alpha = 1.0
        }
        else {
            self.rewardImageView.alpha = 0.2
                        let lighterThemeColor = UIColor(red: 90.0 / 255.0, green: 188.0 / 255.0, blue:  209.0 / 255.0, alpha: 0.1)
            
            self.stepsProgressView.trackColor = lighterThemeColor
            
        }
        
        
        
        
    }

}
