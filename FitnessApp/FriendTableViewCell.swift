//
//  FriendTableViewCell.swift
//  FitnessApp
//
//  Created by Ron Ramirez on 5/28/17.
//  Copyright © 2017 Ron Ramirez. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage

class FriendTableViewCell: UITableViewCell {
    @IBOutlet weak var numberLabel: CircleLabel!
    @IBOutlet weak var friendProfileImageView: CircleImageView!
    @IBOutlet weak var friendStepsCountLabel: UILabel!
    @IBOutlet weak var friendNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellWith(friendKey: String) {
        let currentDate = Date()
        
        DataService.ds.REF_PUBLIC_USERS.child(friendKey).observeSingleEvent(of: .value, with: { (snapshot) in
            //Access current user dictionary
            if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                //Access name
                if let userName = userDict["name"] as? String {
                    self.friendNameLabel.text = userName
                }
                
                //Access user image URL
                if let imageURL = userDict["imageURL"] as? String {
                    print("User  Image URL \(imageURL)")
                    self.friendProfileImageView.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "running"))
                }
            }
        })
        
        //Load user info
        DataService.ds.REF_PUBLIC_USERS.child(friendKey).child("fitnessPosts").child(currentDate.FirebaseDateFormatter()).observe(.value, with: { (snapshot) in
            print("FITNESS", snapshot)
            if let fitnessDataDict = snapshot.value as? Dictionary<String, AnyObject> {
                
                //Access Total Steps
                if let totalSteps = fitnessDataDict["totalSteps"] as? Int {
                    self.friendStepsCountLabel.text = "\(totalSteps) steps"
                }
            }
        })

    }
}
