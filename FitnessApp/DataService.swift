//
//  DataService.swift
//  FitnessApp
//
//  Created by Ron Ramirez on 5/19/17.
//  Copyright © 2017 Ron Ramirez. All rights reserved.
//
import Foundation
import Firebase


//Access Firebase Database url that contains the database - GoogleService-Info.plist
let DB_BASE = FIRDatabase.database().reference()

//Access Firebse Storage
let STORAGE_BASE = FIRStorage.storage().reference()

class DataService  {
    
    static let ds = DataService()
    
    //Database References
    private var _REF_BASE = DB_BASE
    private var _REF_PUBLIC_USERS = DB_BASE.child("publicUsers")
    private var _REF_USERS = DB_BASE.child("users")
    
    //Storage references
    private var _REF_PROFILE_IMAGES = STORAGE_BASE.child("profile-pics")
    
    var REF_BASE : FIRDatabaseReference {
        return _REF_BASE
    }
    
    var REF_PUBLIC_USERS :FIRDatabaseReference {
        return _REF_PUBLIC_USERS
    }
    
    var REF_USERS: FIRDatabaseReference {
        return _REF_USERS
    }
    
    var REF_USER_CURRENT : FIRDatabaseReference {
        let currentUID = FIRAuth.auth()?.currentUser?.uid
        let user = REF_PUBLIC_USERS.child(currentUID!)
        return user
    }
    
    var REF_PROFILE_IMAGES : FIRStorageReference {
        return _REF_PROFILE_IMAGES
    }
    
    //Helper Functions
    
    //Creates a new user in Firebase Database
    func createFirebaseDBUserInPublicUsers(uid : String, userData : Dictionary<String, String>) {
        REF_PUBLIC_USERS.child(uid).updateChildValues(userData)
    }
    
    
    func createFirebaseDBUserInUsers(uid : String, userData : Dictionary<String, String>) {
        REF_USERS.child(uid).updateChildValues(userData)
    }
}
