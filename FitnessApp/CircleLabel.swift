//
//  CircleLabel.swift
//  FitnessApp
//
//  Created by Ron Ramirez on 5/28/17.
//  Copyright © 2017 Ron Ramirez. All rights reserved.
//

import UIKit

class CircleLabel: UILabel {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }

}
