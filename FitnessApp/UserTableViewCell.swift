//
//  UserTableViewCell.swift
//  FitnessApp
//
//  Created by Ron Ramirez on 5/28/17.
//  Copyright © 2017 Ron Ramirez. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage

class UserTableViewCell: UITableViewCell {
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userDescriptionLabel: UILabel!
    @IBOutlet weak var followButton: BorderedButton!
    @IBOutlet weak var userImageView : CircleImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellWith(userKey: String) {
        DataService.ds.REF_PUBLIC_USERS.child(userKey).observeSingleEvent(of: .value, with: { (snapshot) in
            
            //Access current user dictionary
            if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                //Access name
                if let userName = userDict["name"] as? String {
                    self.userNameLabel.text = userName
                }
                
                //Access user image URL
                if let imageURL = userDict["imageURL"] as? String {
                    print("User  Image URL \(imageURL)")
                    self.userImageView.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "running"))
                }

            }
        })
        
        
    }

}
