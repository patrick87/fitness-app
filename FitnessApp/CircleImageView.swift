//
//  CircleImageView.swift
//  Filhoops
//
//  Created by Ron Ramirez on 11/28/16.
//  Copyright © 2016 Mochi Apps. All rights reserved.
//

import UIKit

class CircleImageView: UIImageView {

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
        self.layer.borderWidth = 3.0;
        
        let dimmedThemeColor = UIColor(red: 90.0 / 255.0, green: 188.0 / 255.0, blue:  209.0 / 255.0, alpha: 0.5)
        self.layer.borderColor = dimmedThemeColor.cgColor;
    }

}
