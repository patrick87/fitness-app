//
//  MessageTableViewCell.swift
//  FitnessApp
//
//  Created by Ron Ramirez on 5/29/17.
//  Copyright © 2017 Ron Ramirez. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var messageSenderNameLabel: UILabel!
    @IBOutlet weak var messageTimeStampLabel: UILabel!
    @IBOutlet weak var messageTextLabel: UITextView!
    @IBOutlet weak var messageSenderProfileImageView: CircleImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCellWith(message: Message) {
        
        //Load message and time stamp
        self.messageTimeStampLabel.text = message.timeStamp
        self.messageTextLabel.text = message.messageText
        
        //Load user
        DataService.ds.REF_PUBLIC_USERS.child(message.messageSenderKey).observeSingleEvent(of: .value, with: { (snapshot) in
            
            //Access current user dictionary
            if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                //Access name
                if let userName = userDict["name"] as? String {
                    self.messageSenderNameLabel.text = userName
                }
                
                //Access user image URL
                if let imageURL = userDict["imageURL"] as? String {
                    print("User  Image URL \(imageURL)")
                    self.messageSenderProfileImageView.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "running"))
                }
            }
        })

    
    }

}
