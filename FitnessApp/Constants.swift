
//
//  Constants.swift
//  FitnessApp
//
//  Created by Ron Ramirez on 5/21/17.
//  Copyright © 2017 Ron Ramirez. All rights reserved.
//

import Foundation
import UIKit
import FCAlertView
import CoreGraphics

//Blue color
let themeColor = UIColor(red: 90.0 / 255.0, green: 188.0 / 255.0, blue:  209.0 / 255.0, alpha: 1.0)

//Alert Views
func alertViewWarning(withMessage message : String, viewToShow : UIViewController) {
    let customView = FCAlertView()
    customView.makeAlertTypeCaution()
    customView.showAlert(inView: viewToShow, withTitle: "Warning", withSubtitle: message, withCustomImage: UIImage(named:"close-round"), withDoneButtonTitle: nil, andButtons: [])
    customView.hideDoneButton = true
    customView.addButton("Done", withActionBlock: { () in
        customView.colorScheme = customView.flatOrange
    })
    
}

func alertViewSuccess(withMessage message : String, viewToShow : UIViewController) {
    let customView = FCAlertView()
    customView.makeAlertTypeSuccess()
    customView.showAlert(inView: viewToShow, withTitle: "Success", withSubtitle: message, withCustomImage: UIImage(named:"checkmark-round"), withDoneButtonTitle: "Ok", andButtons: [])
}

public func roundedPolygonPath(rect: CGRect, lineWidth: CGFloat, sides: NSInteger, cornerRadius: CGFloat, rotationOffset: CGFloat = 0) -> UIBezierPath {
    let path = UIBezierPath()
    let theta: CGFloat = CGFloat(2.0 * M_PI) / CGFloat(sides) // How much to turn at every corner
    let offset: CGFloat = cornerRadius * tan(theta / 2.0)     // Offset from which to start rounding corners
    let width = min(rect.size.width, rect.size.height)        // Width of the square
    
    let center = CGPoint(x: rect.origin.x + width / 2.0, y: rect.origin.y + width / 2.0)
    
    // Radius of the circle that encircles the polygon
    // Notice that the radius is adjusted for the corners, that way the largest outer
    // dimension of the resulting shape is always exactly the width - linewidth
    let radius = (width - lineWidth + cornerRadius - (cos(theta) * cornerRadius)) / 2.0
    
    // Start drawing at a point, which by default is at the right hand edge
    // but can be offset
    var angle = CGFloat(rotationOffset)
    
    let corner = CGPoint(x: center.x + (radius - cornerRadius) * cos(angle),y: center.y + (radius - cornerRadius) * sin(angle))
    path.move(to: CGPoint(x:corner.x + cornerRadius * cos(angle + theta),y: corner.y + cornerRadius * sin(angle + theta)))
    
    for _ in 0..<sides {
        angle += theta
        
        let corner = CGPoint(x:center.x + (radius - cornerRadius) * cos(angle),y: center.y + (radius - cornerRadius) * sin(angle))
        let tip = CGPoint(x: center.x + radius * cos(angle),y: center.y + radius * sin(angle))
        let start = CGPoint(x: corner.x + cornerRadius * cos(angle - theta),y: corner.y + cornerRadius * sin(angle - theta))
        let end = CGPoint(x: corner.x + cornerRadius * cos(angle + theta),y: corner.y + cornerRadius * sin(angle + theta))
        
        path.addLine(to: start)
        path.addQuadCurve(to: end, controlPoint: tip)
    }
    
    path.close()
    
    // Move the path to the correct origins
    let bounds = path.bounds
    let transform = CGAffineTransform(translationX: -bounds.origin.x + rect.origin.x + lineWidth / 2.0, y: -bounds.origin.y + rect.origin.y + lineWidth / 2.0)
    path.apply(transform)
    
    return path
}

