//
//  FitnessData+CoreDataProperties.swift
//  
//
//  Created by Ron Ramirez on 5/22/17.
//
//

import Foundation
import CoreData

extension FitnessData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FitnessData> {
        return NSFetchRequest<FitnessData>(entityName: "FitnessData")
    }

    @NSManaged public var date: NSDate?
    @NSManaged public var totalCalories: Int32
    @NSManaged public var totalSteps: Int32
    @NSManaged public var averageTemperature: [Int]
    @NSManaged public var averageHeartRate: [Int]

}
