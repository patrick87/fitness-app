//
//  AchievementsViewController.swift
//  FitnessApp
//
//  Created by Ron Ramirez on 5/30/17.
//  Copyright © 2017 Ron Ramirez. All rights reserved.
//

import UIKit
import CoreData

class AchievementsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateLabel: UILabel!
    
    // Fetch Results
    var weeklyResultsArray = [FitnessData]()
    var currentDate = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Load data within a week
        loadAllDataWithinWeek()
        updateLabelBasedOnArray()
        
        for fitness in self.weeklyResultsArray {
            let date : Date = fitness.date! as Date
            print("Fitness Data - ", date.MonthDayDateFormatter(), fitness.totalSteps, fitness.totalCalories)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        self.currentDate = (Calendar.current as NSCalendar).date(byAdding: .day, value: 7, to: currentDate, options: [])!
        loadAllDataWithinWeek()
        self.tableView.reloadData()
        updateLabelBasedOnArray()
        
    }
    
    @IBAction func lastButtonPressed(_ sender: UIButton) {
        self.currentDate = (Calendar.current as NSCalendar).date(byAdding: .day, value: -7, to: currentDate, options: [])!
        loadAllDataWithinWeek()
        self.tableView.reloadData()
        updateLabelBasedOnArray()
        
    }
    
    //MARK: Core Data Functions
    //Core Data functions
    func loadAllDataWithinWeek() {
        print("Load Fitness Data Based on Week")
        let app = UIApplication.shared.delegate as! AppDelegate
        let context = app.persistentContainer.viewContext //scratch pad
        let currentFitnessDataFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "FitnessData")
        
        // Get the current calendar with local time zone
        var calendar = Calendar.current
        calendar.timeZone = NSTimeZone.local
        
        // Get today's beginning & end
        let calculatedDate = (Calendar.current as NSCalendar).date(byAdding: .day, value: 1, to: currentDate, options: [])!
        
        let dateFrom = calendar.startOfDay(for: calculatedDate) // eg. 2016-10-10 00:00:00
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute],from: dateFrom)
        components.day! += -7
        let dateTo = calendar.date(from: components)! // eg. 2016-10-11 00:00:00
        // Note: Times are printed in UTC. Depending on where you live it won't print 00:00:00 but it will work with UTC times which can be converted to local time
        
        // Set predicate as date being today's date
        let datePredicate = NSPredicate(format: "(%@ >= date) AND (date >= %@)", argumentArray: [dateFrom, dateTo])
        
        //Sort descriptors
        let sectionSortDescriptor = NSSortDescriptor(key: "date", ascending: true)
        let sortDescriptors = [sectionSortDescriptor]
        
        currentFitnessDataFetch.sortDescriptors = sortDescriptors
        currentFitnessDataFetch.predicate = datePredicate
        
        do {
            print("Do Block")
            let fetchedFitnessData = try context.fetch(currentFitnessDataFetch) as! [FitnessData]
            self.weeklyResultsArray = fetchedFitnessData
            print(fetchedFitnessData)
        } catch {
            fatalError("Failed to fetch current fitness data: \(error.localizedDescription)")
        }
    }
    
    //MARK: Helper Functions
    func updateLabelBasedOnArray() {
        //Set up label
        guard self.weeklyResultsArray.count >= 5 else {
            print("Not enough day")
            return
        }
        let firstDayOfTheWeekdate  = (self.weeklyResultsArray.first!.date!) as Date
        let lastDayOfTheWeekdate = (self.weeklyResultsArray.last!.date!) as Date
        
        
        self.dateLabel.text = "\(firstDayOfTheWeekdate.MonthDayDateFormatter()) - \(lastDayOfTheWeekdate.MonthDayDateFormatter())"
    }
}

//MARK: TableViewDelegate methods

extension AchievementsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weeklyResultsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentFitnessData = self.weeklyResultsArray[indexPath.row]
        
        if let rewardCell = tableView.dequeueReusableCell(withIdentifier: "AchievementCell") as? RewardTableViewCell {
            rewardCell.configureCellWith(fitnessData: currentFitnessData)
            return rewardCell
        }
        return UITableViewCell()

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
}
