//
//  BorderedButton.swift
//  FitnessApp
//
//  Created by Ron Ramirez on 5/28/17.
//  Copyright © 2017 Ron Ramirez. All rights reserved.
//

import UIKit

class BorderedButton: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.borderWidth = 1.0;
        self.layer.cornerRadius = 5.0;
        self.layer.borderColor = themeColor.cgColor
    }
}
