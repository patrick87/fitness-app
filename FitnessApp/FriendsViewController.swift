//
//  FriendsViewController.swift
//  FitnessApp
//
//  Created by Ron Ramirez on 5/26/17.
//  Copyright © 2017 Ron Ramirez. All rights reserved.
//

import UIKit
import Firebase

class FriendsViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var friendSelectorView: UIView!
    @IBOutlet weak var allUserSelectorView: UIView!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    //MARK: Variables
    var isShowingAllUsers = false
    var allUserKeys = [String]()
    var friendUserKeys = [String]()
    
    //MARK: View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Initialize dateLabel
        let currentDate = Date()
        self.dateLabel.text = currentDate.MonthDayDateFormatter()
        
        //Load Firebase data
        loadAllFriends()
        loadAllUsers()
    }
    
    //MARK: Storyboard
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToProfileVC" {
            let playerVC = segue.destination as! ProfileViewController
            if let userSelectedKey = sender as? String {
                playerVC.currentUserKey = userSelectedKey
            }
        }
    }
    
    //MARK: IBActions

    @IBAction func allUsersButtonPressed(_ sender: UIButton) {
        
        //Set up UI to show which button is selected
        self.allUserSelectorView.isHidden = false
        self.friendSelectorView.isHidden = true
        
        //Setup tableview
        isShowingAllUsers = true
        self.tableView.reloadData()
        
    }
    
    @IBAction func friendsButtonPressed(_ sender: UIButton) {
        //Set up UI to show which button is selected
        self.allUserSelectorView.isHidden = true
        self.friendSelectorView.isHidden = false
        
        //Set up tableView
        isShowingAllUsers = false
        self.tableView.reloadData()
    }
    
    //MARK: Firebase Functions
    
    func loadAllUsers() {
        
        DataService.ds.REF_PUBLIC_USERS.observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                //Clear all user keys to prevent duplicates
                self.allUserKeys = []
                for snap in snapshots {
                    
                    //Assure current user doesn't get added
                    if snap.key != DataService.ds.REF_USER_CURRENT.key {
                        print("FriendsViewController -loadAllUsers() - \(snap)")
                        self.allUserKeys.append(snap.key)
                    }
                }
                self.tableView.reloadData()
            }
        })
    }
    
    func loadAllFriends() {
        DataService.ds.REF_USER_CURRENT.child("friends").observe(.value, with: { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                
                //Clear all user keys to prevent duplicates
                self.friendUserKeys = []
                for snap in snapshots {
                    //Assure current user doesn't get added
                    if snap.key != DataService.ds.REF_USER_CURRENT.key {
                        print("FriendsViewController -loadAllFriends() - \(snap)")
                        self.friendUserKeys.append(snap.key)
                    }
                }
                self.tableView.reloadData()
            }
        })
        
    }
    
    //MARK: Target Actions
    func followUserButtonPressed(button : UIButton) {
        //
        //        CGPoint touchPoint = [acceptButton convertPoint:CGPointZero toView:self.tableView];
        //        NSIndexPath *clickedButtonIndexPath = [self.tableView indexPathForRowAtPoint:touchPoint];
        //        NSLog(@"Accept Button - NSIndex Path Row %ld", (long) clickedButtonIndexPath.row);
        let zeroPoint = CGPoint(x: 0, y: 0)
        let touchPoint = button.convert(zeroPoint, to: self.tableView)
        let clickedbuttonIndexPath = self.tableView.indexPathForRow(at: touchPoint)
        print("Clicked Button at Index Path", clickedbuttonIndexPath!.row)
        
        let friendDict = [self.allUserKeys[clickedbuttonIndexPath!.row] : true]
        
        //Save value to current user's friends
        DataService.ds.REF_USER_CURRENT.child("friends").updateChildValues(friendDict)
        //setValue(true, forKey: self.allUserKeys[clickedbuttonIndexPath!.row])
    }
    
}

extension FriendsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (isShowingAllUsers) {
            //All Users
            let currentUserKey = self.allUserKeys[indexPath.row]
            if let userCell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell") as? UserTableViewCell {
                userCell.configureCellWith(userKey: currentUserKey)
                
                //Track button
                userCell.followButton.tag = indexPath.row
                
                //Add Selector to button
                userCell.followButton.addTarget(self, action:#selector(followUserButtonPressed(button:)), for: .touchUpInside)
                return userCell
            }
        }else {
            //All friends
            let currentFriendKey = self.friendUserKeys[indexPath.row]
            if let friendCell = tableView.dequeueReusableCell(withIdentifier: "FriendTableViewCell") as? FriendTableViewCell {
                friendCell.configureCellWith(friendKey: currentFriendKey)
                friendCell.numberLabel.text = "\(indexPath.row + 1)"
                return friendCell
            }
        }
        //let cell = tableView.dequeueReusableCell(withIdentifier: "FriendTableViewCell", for: indexPath) as! FriendTableViewCell
        //cell.numberLabel.text = "\(indexPath.row + 1)"
        
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (isShowingAllUsers) {
            return self.allUserKeys.count
        }else {
            return self.friendUserKeys.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (!isShowingAllUsers) {
            let friendUserKey = self.friendUserKeys[indexPath.row]
            self.performSegue(withIdentifier: "goToProfileVC", sender: friendUserKey)
        }
     
    }
}
