//
//  BlueToothServices.swift
//  FitnessApp
//
//  Created by Ron Ramirez on 5/20/17.
//  Copyright © 2017 Ron Ramirez. All rights reserved.
//

import Foundation

enum Services: String {
    case heartMonitor = "CE4CFF01-B85D-49AA-8E03-0D34779A6EEF"
    case stepsMonitor = "3E18B512-D819-4550-8343-F9EFFDA2F896"
    case temperatureMonitor = "4B5CF42E-3224-43B4-AFA2-8A0917B34856"
}

enum Characteristics: String {
    case heartRate = "7821C91E-A551-4907-A5E0-F6CB64AC0A4B"
    case stepsCount = "EE6134CF-F907-45CD-B259-2AB681CA6B32"
    case temperatureStat = "2BCE8CF5-F03E-4EB2-BB35-77C87AC5F1A4"
}
