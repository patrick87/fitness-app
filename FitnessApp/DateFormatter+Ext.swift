//
//  DateFormatter+Ext.swift
//  Budget Pig
//
//  Created by Ron Ramirez on 9/12/16.
//  Copyright © 2016 Mochi Apps. All rights reserved.
//

import Foundation


extension Date {
    
    func MonthDayDateFormatter() -> String {
        return DateFormatter.MonthDayDateFormatter.string(from: self)
    }
    
    func DayDateFormatter() -> String {
        return DateFormatter.DayDateFormatter.string(from: self)
    }
    
    func MonthDateFormatter() -> String {
        return DateFormatter.MonthDateFormatter.string(from: self)
    }
    
    func FirebaseDateFormatter() -> String {
        return DateFormatter.FirebaseDateFormatter.string(from: self)
    }
    
    func CommentDateFormatter() -> String {
        return DateFormatter.CommenDateFormatter.string(from: self)
    }

    func WeekdayDateFormatter() -> String {
        return DateFormatter.WeekdayDateFormatter.string(from: self)
    }
}

// Formatter for Short-Style Date E.G. 10/11/2016 3:11 P.M
extension DateFormatter {
    
    fileprivate static let MonthDayDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM d, YYYY"
        return formatter
    }()
    
    fileprivate static let DayDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE d"
        return formatter
    }()
    
    fileprivate static let WeekdayDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        return formatter
    }()
    
    
    fileprivate static let MonthDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM, YYYY"
        return formatter
    }()
    
    //yyyy-MM-dd
    
    //Firebase Date Formatter
    fileprivate static let FirebaseDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    
    //Comment Date Formatter
    //h:mm a
    
    fileprivate static let CommenDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        return formatter
    }()
    
}



