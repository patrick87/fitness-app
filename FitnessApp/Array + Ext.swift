//
//  Array + Ext.swift
//  FitnessApp
//
//  Created by Ron Ramirez on 5/28/17.
//  Copyright © 2017 Ron Ramirez. All rights reserved.
//

import Foundation

extension Array where Element == Int {
    /// Returns the sum of all elements in the array
    var total: Element {
        return reduce(0, +)
    }
    /// Returns the average of all elements in the array
    var average: Double {
        return isEmpty ? 0 : Double(reduce(0, +)) / Double(count)
    }
}
