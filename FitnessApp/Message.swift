//
//  Model.swift
//  FitnessApp
//
//  Created by Ron Ramirez on 5/29/17.
//  Copyright © 2017 Ron Ramirez. All rights reserved.
//

import Foundation
import Firebase

class Message {
    
    //Properties
    private var _messageTimeStamp : String?
    private var _messageText : String?
    private var _messageSenderKey : String?
    
    //Accessors/Mutators
    var timeStamp: String {
        if _messageTimeStamp != nil {
            return _messageTimeStamp!
        }else {
            return "0:00"
        }
    }
    
    var messageText: String {
        if _messageText != nil {
            return _messageText!
        }else {
            return "Empty Message"
        }
    }

    var messageSenderKey: String {
        if _messageSenderKey != nil {
            return _messageSenderKey!
        }else {
            return "Empty Sender Key"
        }
    }

    //Initializers
    init(messageData : Dictionary<String, AnyObject>) {
        if let senderKey = messageData["senderKey"] as? String {
            self._messageSenderKey = senderKey
        }
        
        if let messageText = messageData["message"] as? String {
            self._messageText = messageText
        }
        
        if let time = messageData["timeStamp"] as? String {
            self._messageTimeStamp = time
        }
    }
}

