//
//  ViewController.swift
//  FitnessApp
//
//  Created by Ron Ramirez on 5/18/17.
//  Copyright © 2017 Ron Ramirez. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Firebase
import GoogleSignIn
import Pastel



class SignupViewController: UIViewController {

    
    //MARK:UITextFields
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    //Typealias
    typealias nameID  = String
    typealias emailID = String
    typealias passwordID = String
    
    //MARK: View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        self.navigationController?.isNavigationBarHidden = true
        
        let pastelView = PastelView(frame: view.bounds)
        
        // Custom Direction
        pastelView.startPastelPoint = .bottomLeft
        pastelView.endPastelPoint = .topRight
        
        // Custom Duration
        pastelView.animationDuration = 3.0
        
        // Custom Color
        pastelView.setColors([UIColor(red: 156/255, green: 39/255, blue: 176/255, alpha: 1.0),
                              UIColor(red: 255/255, green: 64/255, blue: 129/255, alpha: 1.0),
                              UIColor(red: 123/255, green: 31/255, blue: 162/255, alpha: 1.0),
                              UIColor(red: 32/255, green: 76/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 32/255, green: 158/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 90/255, green: 120/255, blue: 127/255, alpha: 1.0),
                              UIColor(red: 58/255, green: 255/255, blue: 217/255, alpha: 1.0)])
        
        pastelView.startAnimation()
        view.insertSubview(pastelView, at: 0)
    }
    
    //MARK: IBActions
    @IBAction func facebuttonButtonPressed(_ sender: UIButton) {
        print("Facebook Button Pressed")
        
        FBSDKLoginManager().logIn(withReadPermissions: ["email" , "public_profile"], from: self) {
            (result, error) in
            
            //Print error
            if error != nil {
                print("Custom Login Failed , \(error!.localizedDescription)")
                return
            }
            self.parseUsersFacebook()
        }
    }
    
    @IBAction func googleButtonPressed(_ sender: Any) {
        print("Google Button Pressed!")
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func signupButtonPressed(_ sender: Any) {
        print("Signup button pressed!")
        
        //Handle name
        guard let name = self.nameTextField.text ,!name.isEmpty else {
            print("Please enter a valid name")
            alertViewWarning(withMessage: "Please enter a valid name", viewToShow: self)
            return
        }
        
        //Check if name have numbers
        let numberCharacters = NSCharacterSet.decimalDigits
        guard name.rangeOfCharacter(from: numberCharacters) == nil else {
            print("Please enter a valid name without numbers!")
            alertViewWarning(withMessage: "Please enter a valid name without numbers!", viewToShow: self)
            return
        }
        
        //Handle email
        guard let email = self.emailTextField.text , !email.isEmpty else {
            print("Please make sure email text field is not empty")
            alertViewWarning(withMessage: "Please make sure email text field is not empty", viewToShow: self)
            return
        }
        
        //Handle email format
        if !isValidEmail(email: email) {
            print("Email is badly formatted")
            alertViewWarning(withMessage: "Email is badly formatted.", viewToShow: self)
            return
        }
        
        //Handle password
        guard let password = self.passwordTextField.text, !password.isEmpty,
        let confirmPassword = self.confirmPasswordTextField.text, !confirmPassword.isEmpty else {
            print("Please make sure password fields are not empty")
            alertViewWarning(withMessage: "Please make sure password fields are not empty", viewToShow: self)
            return
        }
        
        //Check if password is the same
        if password != confirmPassword {
            print("Passwords does not match!")
            alertViewWarning(withMessage: "Passwords does not match!", viewToShow: self)
            return
        }
        
        //Sign in
        self.signInEmailUser(name: name, email: email, password: password)
    }
    
    //MARK: Social Media Login Functions
    
    func signInEmailUser(name : nameID, email: emailID, password : passwordID) {
        FIRAuth.auth()?.createUser(withEmail: email, password: password, completion: { (user, error) in
            
            //Handle error
            if (error != nil) {
                print("Email sign in - error sign in", error!.localizedDescription)
                return
            }
            
            print("Successfully logged in as email user!")
            
            //Make sure there's a current user
            guard let currentUID = user?.uid else {
                print("Current User Error!")
                return
            }
            
            //Firebase Google Sign-in
            //Create a public user dictionary to save to public users
            let publicUserDict = [
                "name" : name
            ]
            
            //Save to public users Firebase database
            DataService.ds.createFirebaseDBUserInPublicUsers(uid: currentUID, userData: publicUserDict)
            print(publicUserDict)
            
            //Create a private user dictionary to store in Firebase database
            let privateUserDict = [
                "name" : name,
                "email" : email,
                "provider" : "Firebase email"
            ]
            
            DataService.ds.createFirebaseDBUserInUsers(uid: currentUID, userData: privateUserDict)
            print(privateUserDict)
            
            //Segue to new view controller
            self.performSegue(withIdentifier: "showMainVC", sender: nil)
        
        })
    }

    func parseUsersFacebook() {
        
        //Set up Firebase user for Facebook user
        let accessToken = FBSDKAccessToken.current()
        
        guard let accessTokenString = accessToken?.tokenString else { return }
        
        let credentials = FIRFacebookAuthProvider.credential(withAccessToken: accessTokenString)
        
        FIRAuth.auth()?.signIn(with: credentials, completion: { (user, error) in
            if error != nil {
                //Print error
                print("Something went wrong with our FB user: ", error!.localizedDescription)
                return
            }
            
            //Print user name
            if let userName = user?.displayName{
                print("Successfully logged in with our user", userName)
            }
        })
        
        //Set up Graph Request
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields" : "id, name, email, picture"]).start {
            (connection, result, err) in
            
            //Check for error
            if err != nil {
                print("Failed to start graph request: ", err!.localizedDescription)
                return
            }
            
            //Make sure there's a current user
            guard let currentUID = FIRAuth.auth()?.currentUser?.uid else {
                print("Current User Error!")
                return
            }
            
            //Parse Facebook Dictionary
            guard let facebookDict = result as? Dictionary<String, AnyObject>,
                let id = facebookDict["id"] as? String,
                let name = facebookDict["name"] as? String,
                let email = facebookDict["email"] as? String else {
                    print("Parsing Facebook Dictionary Failed!")
                    return
            }
            
            //Create an image url based on facebook id
            let facebookProfileImageUrl = "http://graph.facebook.com/\(id)/picture?type=large"
            
            //Create a public user dictionary to save to public users
            let publicUserDict = [
                "name" : name,
                "imageURL" : facebookProfileImageUrl
            ]
            
            //Save to public users Firebase database
            DataService.ds.createFirebaseDBUserInPublicUsers(uid: currentUID, userData: publicUserDict)
            print(publicUserDict)
            
            //Create a private user dictionary to store in Firebase database
            let privateUserDict = [
                "name" : name,
                "email" : email,
                "imageURL" : facebookProfileImageUrl,
                "provider" : credentials.provider
            ]
            
            //Save to users Firebasedatbase
            DataService.ds.createFirebaseDBUserInUsers(uid: currentUID, userData: privateUserDict)
            print(privateUserDict)
            
            //Segue to new view controller
            self.performSegue(withIdentifier: "showMainVC", sender: nil)
        }
    }
    
    //MARK: Email Validation Functions
    func isValidEmail(email: emailID) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
}


//MARK: UITextFieldDelegate

extension SignupViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}


//MARK: Google Sign Delgate
extension SignupViewController : GIDSignInUIDelegate, GIDSignInDelegate  {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        
        //Google Sign in
        if let error = error {
            print("Google Signin Error - ",error.localizedDescription)
            return
        }
        print("Successfully logged into Google")
        
        //Google Sign in
        //Parse Google Dictionary
        guard let imageURL = user.profile.imageURL(withDimension: 200),
            let fullName = user.profile.name,
            let email = user.profile.email else {
                print("Parsing Google Dictionary Failed")
                return
        }
        guard let authentication = user.authentication else { return }
        let credential = FIRGoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                          accessToken: authentication.accessToken)
        
        //Firebase sign in
        FIRAuth.auth()?.signIn(with: credential) { (user, error) in
            //Test out error
            if let error = error {
                print("Firebase Signin Error", error.localizedDescription)
                return
            }
            
            //Make sure there's a current user
            guard let currentUID = user?.uid else {
                print("Current User Error!")
                return
            }
            
            //Firebase Google Sign-in
            //Create a public user dictionary to save to public users
            let publicUserDict = [
                "name" : fullName,
                "imageURL" : imageURL.absoluteString
            ]
            
            //Save to public users Firebase database
            DataService.ds.createFirebaseDBUserInPublicUsers(uid: currentUID, userData: publicUserDict)
            print(publicUserDict)
            
            //Create a private user dictionary to store in Firebase database
            let privateUserDict = [
                "name" : fullName,
                "email" : email,
                "imageURL" : imageURL.absoluteString,
                "provider" : credential.provider
            ]
            
            DataService.ds.createFirebaseDBUserInUsers(uid: currentUID, userData: privateUserDict)
            print(privateUserDict)
            
            //Segue to new view controller
            self.performSegue(withIdentifier: "showMainVC", sender: nil)
        }
    }
}






