//
//  FitnessTrackerViewController.swift
//  FitnessApp
//
//  Created by Ron Ramirez on 5/20/17.
//  Copyright © 2017 Ron Ramirez. All rights reserved.
//

import UIKit
import KDCircularProgress
import CoreBluetooth
import SVProgressHUD
import CoreData
import FCAlertView
import FBSDKShareKit


class FitnessTrackerViewController: UIViewController {
    @IBOutlet weak var progress: KDCircularProgress!
    @IBOutlet weak var stepsCounterLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var heartRateLabel: UILabel!
    @IBOutlet weak var calorieCountLabel: UILabel!
    @IBOutlet weak var currentDateLabel: UILabel!
    @IBOutlet weak var runButton: UIButton!
    var currentSteps : Int = 0
    
    var currentFitnessData : FitnessData?
    var fitnessDataArray = [FitnessData]()
    
    //CoreBluetooth
    var centralManager: CBCentralManager?
    var myPeripheral: CBPeripheral?
    
    //Characteristics
    var heartRateCharacteristic: CBCharacteristic?
    var stepsCountCharacteristic: CBCharacteristic?
    var temperatureStatCharacteristic: CBCharacteristic?
    
    //Check monitor services status
    var foundHeartService = false, foundTemperatureService = false, foundStepsMonitorService = false
    
    //MARK: View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        //Initialize Central Manager
        centralManager = CBCentralManager(delegate: self, queue: nil, options: nil)
        
        //Delete all current Data
        //clearAllCoreData()
        
        //Add dummy date for the next month
        //addDummyDataForTheCurrentMonth()
        
        //Core Data
        //Check for Current Core Data
        loadCurentFitnessData()
        
        fetchAllFitnessData()
        
        guard let fitnessData = self.currentFitnessData else {
            print("Fitness Data doesn't exist!")
            return
        }
        
        print("Total Steps \(fitnessData.totalSteps)")
        print("Fitness Data ", fitnessData)
        let date : Date = fitnessData.date! as Date
        print("Current Date ", date.MonthDayDateFormatter())
        self.stepsCounterLabel.text = "\(fitnessData.totalSteps)"
        self.currentSteps = Int(fitnessData.totalSteps)
        progress.angle = Double((currentSteps * 360) / 10000)
        calorieCountLabel.text = "\(Int(Double(currentSteps) * 0.045))"
        currentDateLabel.text = date.MonthDayDateFormatter()
        
        print(self.fitnessDataArray)
        for fitness in self.fitnessDataArray {
            let date : Date = fitness.date! as Date
            print("Fitness Data - ", date.MonthDayDateFormatter(), fitness.totalSteps, fitness.totalCalories)
            print("Heart Rates" , fitnessData.averageHeartRate)
            print("Body Temperatures", fitnessData.averageTemperature)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Initialize Central Manager
        print("View Will Appear!")
        
        centralManager = CBCentralManager(delegate: self, queue: nil, options: nil)
        
        //Disable run button until connected blue tooth service
        SVProgressHUD.show(withStatus: "Synchronizing Bluetooth")
        self.runButton.isEnabled = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("View Did Disappear!")
        SVProgressHUD.dismiss()
        
        //Save to Core Data
        saveCoreDataToFirebase()
    }
    
    //MARK: Core Data Functions
    func loadCurentFitnessData() {
        print("load Fitness Data")
        let app = UIApplication.shared.delegate as! AppDelegate
        let context = app.persistentContainer.viewContext //scratch pad
        let currentFitnessDataFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "FitnessData")
        
        // Get the current calendar with local time zone
        var calendar = Calendar.current
        calendar.timeZone = NSTimeZone.local
        
        // Get today's beginning & end
        let dateFrom = calendar.startOfDay(for: Date()) // eg. 2016-10-10 00:00:00
        var components = calendar.dateComponents([.year, .month, .day, .hour, .minute],from: dateFrom)
        components.day! += 1
        let dateTo = calendar.date(from: components)! // eg. 2016-10-11 00:00:00
        // Note: Times are printed in UTC. Depending on where you live it won't print 00:00:00 but it will work with UTC times which can be converted to local time
        
        // Set predicate as date being today's date
        let datePredicate = NSPredicate(format: "(%@ <= date) AND (date < %@)", argumentArray: [dateFrom, dateTo])
        
        currentFitnessDataFetch.predicate = datePredicate
        //currentFitnessDataFetch.fetchLimit = 1
        
        do {
            print("Do Block")
            let fetchedFitnessData = try context.fetch(currentFitnessDataFetch) as! [FitnessData]
            
            if !fetchedFitnessData.isEmpty {
                print("There is a current FitnessData")
                self.currentFitnessData = fetchedFitnessData.first!
            }else {
                print("There is no Fitness Data")
                
                //Initialize a new one
                self.currentFitnessData = initializeCurrentFitnessData()
                
            }
            print(fetchedFitnessData)
        } catch {
            fatalError("Failed to fetch current fitness data: \(error.localizedDescription)")
        }
    }
    
    func fetchAllFitnessData() {
        print("load Fitness Data")
        let app = UIApplication.shared.delegate as! AppDelegate
        let context = app.persistentContainer.viewContext //scratch pad
        let currentFitnessDataFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "FitnessData")
        
        do {
            print("Do Block")
            let fetchedFitnessData = try context.fetch(currentFitnessDataFetch) as! [FitnessData]
            self.fitnessDataArray = fetchedFitnessData
            print(fetchedFitnessData)
        } catch {
            fatalError("Failed to fetch current fitness data: \(error.localizedDescription)")
        }
    }
    
    func initializeCurrentFitnessData() -> FitnessData {
        //Access managedObjectContex
        let app = UIApplication.shared.delegate as! AppDelegate
        let context = app.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "FitnessData", in: context)!
        let fitnessData = FitnessData(entity: entity, insertInto: context)
        
        fitnessData.date = Date() as NSDate
        fitnessData.totalSteps = 0
        fitnessData.totalCalories = 0
        
        //Add object to context
        context.insert(fitnessData)
        
        do {
            try context.save()
        } catch {
            print("Could not save fitness Data")
        }
        return fitnessData
    }
    
    func clearAllCoreData() {
        // fetch all items in entity and request to delete them
        let fitnessDataRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FitnessData")
        let deleteAllFitnessDataRequest = NSBatchDeleteRequest(fetchRequest: fitnessDataRequest)
        
        // delegate objects
        let myManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let myPersistentStoreCoordinator = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
        
        // perform the delete
        do {
            try myPersistentStoreCoordinator.persistentStoreCoordinator.execute(deleteAllFitnessDataRequest, with: myManagedObjectContext)
        } catch let error as NSError {
            print(error)
        }
    }
    
    func addDummyDataForTheCurrentMonth() {
        let currentDate = Date()
        for x in 1...60 {
            let calculatedDate = (Calendar.current as NSCalendar).date(byAdding: .day, value: x, to: currentDate, options: [])!
            initializeDummyFitnessData(dateToAdd: calculatedDate)
        }
    }
    
    func initializeDummyFitnessData(dateToAdd: Date) {
        
        //Access managedObjectContex
        let app = UIApplication.shared.delegate as! AppDelegate
        let context = app.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "FitnessData", in: context)!
        let fitnessData = FitnessData(entity: entity, insertInto: context)
        
        //Initialize with random numbers
        let randomSteps = Int(arc4random_uniform(UInt32(15000)))
        
        fitnessData.date = dateToAdd as NSDate
        fitnessData.totalSteps = Int32(randomSteps)
        fitnessData.totalCalories = Int32(Double(randomSteps) * 0.045)
        
        //Add object to context
        context.insert(fitnessData)
        
        do {
            try context.save()
        } catch {
            print("Could not save fitness Data")
        }
    }
    
    //MARK: IBAction
    @IBAction func runningButtonPressed(_ sender: Any) {
        print("Running ButtonPressed!")
        getSteps()
        getTemp()
        
        //Guard fitness data exists
        guard let fitnessData = self.currentFitnessData else {
            print("Fitness Data doesn't exist!")
            return
        }
        
        progress.angle = Double((currentSteps * 360) / 10000)
        stepsCounterLabel.text = "\(Int(currentSteps))"
        fitnessData.totalSteps = Int32(Int(currentSteps))
        calorieCountLabel.text = "\(Int(Double(currentSteps) * 0.045))"
        ad.saveContext()
        if (currentSteps > 10000) {
            
            guard let screenShot = renderImageFromMap(view: self.view, frame: self.view.frame) else{
                return
            }
            let fbsdkPhoto = FBSDKSharePhoto(image: screenShot, userGenerated: true)
            let fbsdkPhotoContent = FBSDKSharePhotoContent()
            fbsdkPhotoContent.photos = [fbsdkPhoto!]
            FBSDKShareDialog.show(from: self, with: fbsdkPhotoContent, delegate: nil)
            
        }
        
    }
    
    func renderImageFromMap(view:UIView, frame:CGRect) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(frame.size,true,0)
        let context: CGContext = UIGraphicsGetCurrentContext()!
        view.layer.render(in: context)
        view.draw(frame)
        let renderedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return renderedImage
    }

    
    //MARK: Firebase Functions
    
    func saveCoreDataToFirebase() {
        //Guard fitness data exists
        guard let fitnessData = self.currentFitnessData else {
            print("Fitness Data doesn't exist!")
            return
        }
        
        let averageHeartRate = fitnessData.averageHeartRate.average
        let averageBodyTemp = fitnessData.averageTemperature.average
        let fitnessDate = fitnessData.date! as Date
        let fitnessDataDict : [String : Any] = [
            "averageHeartRate" : averageHeartRate,
            "averageBodyTemp" : averageBodyTemp,
            "totalSteps" : Int(fitnessData.totalSteps)
        ]
    
        //Save to Firebase
        DataService.ds.REF_USER_CURRENT.child("fitnessPosts").child(fitnessDate.FirebaseDateFormatter()).updateChildValues(fitnessDataDict)
    }
}


//MARK: Core Bluetooth

extension FitnessTrackerViewController : CBCentralManagerDelegate, CBPeripheralDelegate {
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == .poweredOn {
            central.scanForPeripherals(withServices: nil, options: nil)
        }
        else {
            print("Error with BLE")
            SVProgressHUD.showError(withStatus: "Error with Bluetooth Service, Make sure bluetooth configuration is set up properly")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        for (key, value) in advertisementData {
            if key == "kCBAdvDataLocalName" && (value as! String) == "Patrick's bluetooth device" {
                print("Found Patrick's bluetooth device!!")
                SVProgressHUD.show(withStatus: "Found Patrick's Bluetooth Device")
                myPeripheral = peripheral
                central.stopScan()
                central.connect(myPeripheral!, options: nil)
            }
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Connected to: ", peripheral.name ?? "")
        peripheral.delegate = self
        peripheral.discoverServices(nil)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        for service in peripheral.services! {
            
            if service.uuid.uuidString == Services.heartMonitor.rawValue {
                print("Found heart monitor service")
                peripheral.discoverCharacteristics(nil, for: service)
                foundHeartService = true
            }
            
            if service.uuid.uuidString == Services.stepsMonitor.rawValue {
                print("Found steps monitor service")
                peripheral.discoverCharacteristics(nil, for: service)
                foundStepsMonitorService = true
            }
            
            if service.uuid.uuidString == Services.temperatureMonitor.rawValue {
                print("Found temperature service")
                peripheral.discoverCharacteristics(nil, for: service)
                foundTemperatureService = true
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        for characteristic in service.characteristics! {
            
            if characteristic.uuid.uuidString == Characteristics.heartRate.rawValue {
                heartRateCharacteristic = characteristic
                myPeripheral?.setNotifyValue(true, for: heartRateCharacteristic!)
            }
            
            if characteristic.uuid.uuidString == Characteristics.stepsCount.rawValue {
                stepsCountCharacteristic = characteristic
                getSteps()
            }
            
            if characteristic.uuid.uuidString == Characteristics.temperatureStat.rawValue {
                temperatureStatCharacteristic = characteristic
                getTemp()
            }
            
            //Stop loading
            if foundTemperatureService && foundStepsMonitorService && foundHeartService {
                print("Finished Synchronizing Bluetooth")
                SVProgressHUD.dismiss()
                self.runButton.isEnabled = true
            }else {
                print("Error synchronizing Bluetooth")
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        guard let fitnessData = self.currentFitnessData else {
            print("Fitness Data doesn't exist!")
            return
        }
        
        if characteristic == heartRateCharacteristic {
            if let value = characteristic.value {
                let s = String(bytes: value, encoding: .utf8)
                print("Heart Rate: ", s ?? "")
                self.heartRateLabel.text = "\(s ?? "")"
                if let heartRateInteger = Int(s!) {
                    let heartRateNumber = NSNumber(value:heartRateInteger)
                    fitnessData.averageHeartRate.append(Int(heartRateNumber))
                }
            }
        }
        
        if characteristic == stepsCountCharacteristic {
            if let value = characteristic.value {
                let s = String(bytes: value, encoding: .utf8)
                print("Steps count: ", s ?? "", "on date and time: ", Date().description)
                self.currentSteps += Int(s!)!
            }
        }
        
        if characteristic == temperatureStatCharacteristic {
            if let value = characteristic.value {
                let s = String(bytes: value, encoding: .utf8)
                print("Body temperature: ", s ?? "")
                self.temperatureLabel.text = "\(s ?? "")"
                if let temperatureInteger = Int(s!) {
                    let temperatureNumber = NSNumber(value:temperatureInteger)
                    fitnessData.averageTemperature.append(Int(temperatureNumber))
                }
        
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        if error != nil {
            print(error?.localizedDescription ?? "")
        }
    }
    
    func getSteps() {
        myPeripheral?.readValue(for: stepsCountCharacteristic!)
    }
    
    func getTemp() {
        myPeripheral?.readValue(for: temperatureStatCharacteristic!)
    }
}

